from django.apps import AppConfig


class OptometristConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'optometrist'
