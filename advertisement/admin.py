from django.contrib import admin

from .models import Advertisement


class AdvertisementAdmin(admin.ModelAdmin):
    list_display = ('title', 'advertiser', 'start_date', 'end_date')


admin.site.register(Advertisement, AdvertisementAdmin)
