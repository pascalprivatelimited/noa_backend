from rest_framework.routers import DefaultRouter
from django.urls import path, include
from .views import StorageViewSet,ImageViewSet,EditorImageViewSet,FileViewSet,GetFilesData

router = DefaultRouter()
router.register(r'', StorageViewSet)
# router.register(r'image', ImageViewSet)
router.register(r'editorImage', EditorImageViewSet)
urlpatterns = [
    path('', include(router.urls)),
    path('image', ImageViewSet.as_view({'post':'create'}),name='image'),
    path('file', FileViewSet.as_view({'post':'create'}),name='file'),
    path('getFile', GetFilesData.as_view({'post':'create'}),name='getFile'),

]