from django.test import TestCase
from rest_framework import status
from django.contrib.auth import get_user_model

User = get_user_model()


class OptometristTests(TestCase):
    header = None

    def setUp(self):
        user = User.objects.create_user(email='user@email.com', name='user', password='foo')
        response = self.client.post('/api/v1/token/', {'email': user.email, 'password': 'foo'})
        self.header = {'HTTP_AUTHORIZATION': f'Bearer {response.data["access"]}'}

    file_path = 'http://www.website.com/image.png'
    data = {
        "name": "Ramesh Dhakal",
        "degree": "MBBS",
        "hospital": "Norvic Hospital",
        "location": "Kathmandu",
        "phone": "9800098564",
        "image": file_path,
    }

    def test_home_route_of_optometrists(self):
        # Issue a GET request.
        response = self.client.get('/api/v1/optometrist/')

        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_to_get_first_optometrist(self):
        # Issue a GET request.
        response = self.client.get('/api/v1/optometrist/1/')

        # Check that the response is 200 OK.
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_that_restricted_routes_cannot_be_accessed(self):
        # Issue a post request.
        response = self.client.post('/api/v1/optometrist/')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # Issue a put request.
        response = self.client.put('/api/v1/optometrist/1/')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # Issue a delete request.
        response = self.client.delete('/api/v1/optometrist/1/')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_to_create_update_and_delete_an_optometrist(self):
        # Issue a post request.
        response = self.client.post('/api/v1/optometrist/', self.data, **self.header)

        # Check that the post is created.
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Issue a GET request.
        response = self.client.get(f'/api/v1/optometrist/{response.data["id"]}/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Issue a delete request.
        response = self.client.delete(f'/api/v1/optometrist/{response.data["id"]}/', **self.header)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
