from .views import ArticleViewSet
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import GetPatientListViewSet, GetOptometristListViewSet

router = DefaultRouter()
router.register(r'', ArticleViewSet)

urlpatterns = [
    path('blog/', include(router.urls)),
    path('blogs/patient/', GetPatientListViewSet.as_view(), name="get_patient"),
    path('blogs/optometrist/', GetOptometristListViewSet.as_view(), name="get_optometrist"),
]
