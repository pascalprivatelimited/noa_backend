from .views import AdvertisementViewSet
from django.urls import path, include
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'', AdvertisementViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
