from rest_framework import serializers
from .models import Advertisement


class AdvertisementSerializer(serializers.ModelSerializer):
    start_date = serializers.DateTimeField()
    end_date = serializers.DateTimeField()
    class Meta:
        model = Advertisement
        fields = '__all__'

    def validate(self, data):
        """
        Custom Validations
        """
        if 'start_date' in data and 'end_date' in data and  data['start_date'] > data['end_date']:
            raise serializers.ValidationError({"end_date": "End date must be greater than start date"})
        return data
