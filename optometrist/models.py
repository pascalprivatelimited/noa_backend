from django.db import models


class Optometrist(models.Model):
    name = models.CharField(max_length=250)
    degree = models.CharField(max_length=250)
    hospital = models.CharField(max_length=250)
    location = models.CharField(max_length=250)
    phone = models.CharField(max_length=250)
    image = models.CharField(max_length=250)

    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('created_at',)

    def __str__(self):
        return str(self.pk)
