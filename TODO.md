# TODO : FOR NOA

## Workflow

- [x] Django Project Setup -> Completed

  - Created a django project named "noa"
  - Installed drf , jwt
  - Created a app for blog i.e "api"
  - Created a app for blog i.e "blog"
  - Created a app for account i.e "account"

- [x] Account setup -> Completed

  - Added model , view , serializers and url for user
  - Authentication Setup using JWT
  - Added tests for the user
  - Updated fields for user
  - Updated Tests for Users routes

- [x] Blog setup -> Completed

  - Added model , view , serializers and url for blog
  - Media Storage and CORS Setup
  - Added tests for all the routes of blog

- [x] Optometrist setup -> Completed

  - Added model , view , serializers and url for Optometrist
  - Tested all the routes of optometrist

- [x] General and User Setting -> Completed

  - Added view and serializers for settings
  - Tested the routes for both the settings

- [x] Extra Functionalities -> Completed
  - Added paginations
  - Added search functionality for both blogs and optometrist.
  - Added gunicorn
  - Added gitlab ci/cd to deploy to heroku
