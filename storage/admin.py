from django.contrib import admin

from .models import Storage


class StorageAdmin(admin.ModelAdmin):
    list_display = ('id', 'file')


admin.site.register(Storage, StorageAdmin)
