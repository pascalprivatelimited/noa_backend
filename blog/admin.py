from django.contrib import admin

from .models import Article


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'type', 'image', 'posteddate')


admin.site.register(Article, ArticleAdmin)
