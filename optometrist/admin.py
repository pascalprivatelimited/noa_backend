from django.contrib import admin

from .models import Optometrist


class OptometristAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'location', 'degree', 'phone', 'hospital')


admin.site.register(Optometrist, OptometristAdmin)
