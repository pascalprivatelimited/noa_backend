from django.db import models
from django.dispatch import receiver
from django.db.models.signals import post_save

# Create your models here.


def image_upload_path(instance, filename):
    return '/'.join(['imageStorage', filename])


def file_upload_path(instance, filename):
    return '/'.join(['fileStorage', filename])


class Storage(models.Model):
    file = models.FileField(upload_to=image_upload_path)
    file_name = models.CharField(max_length=255)

    def __str__(self):
        return self.file.name


@receiver(post_save, sender=Storage)
def order_listing_update(sender, instance, created, **kwargs):
    if created:
        instance.file_name = instance.file.name.split("/")[-1].replace(".", "-")
        instance.save()



class Image(models.Model):
    id = models.AutoField(primary_key=True)
    image = models.ImageField(blank=True, null=True,
                              upload_to=image_upload_path)


class File(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=500)
    file = models.FileField(blank=True, null=True, upload_to=file_upload_path)
