from rest_framework import serializers
from .models import Optometrist


class OptometristSerializer(serializers.ModelSerializer):
    class Meta:
        model = Optometrist
        fields = '__all__'
