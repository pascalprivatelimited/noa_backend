from .models import Optometrist
from rest_framework import status
from rest_framework import viewsets, filters
from rest_framework.response import Response
from storage.utils import deleteFileWithUrls
from .serializers import OptometristSerializer
from rest_framework.permissions import IsAuthenticated, AllowAny


class OptometristViewSet(viewsets.ModelViewSet):
    """
    A viewset for viewing, searching and editing Optometrist instances.
    """
    serializer_class = OptometristSerializer
    queryset = Optometrist.objects.all().reverse()
    search_fields = ['name', 'location']
    filter_backends = (filters.SearchFilter,)

    def get_permissions(self):
        """Set custom permissions for each action."""
        if self.action in ["list", "retrieve"]:
            self.permission_classes = [AllowAny, ]
        else:
            self.permission_classes = [IsAuthenticated, ]
        return super().get_permissions()

    def destroy(self, request, *args, **kwargs):
        optometrist = self.get_object()
        deleteFileWithUrls([optometrist.image])
        optometrist.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
