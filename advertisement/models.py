import os
import uuid
from django.db import models


class Advertisement(models.Model):
    title = models.CharField(max_length=250)
    description = models.CharField(max_length=500, blank=True)
    url = models.URLField(max_length=250)
    image = models.CharField(max_length=512),
    advertiser = models.CharField(max_length=250)
    start_date = models.DateTimeField(null=True, blank=True)
    end_date = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = 'Advertisement'
        verbose_name_plural = 'Advertisements'

    def __str__(self):
        return str(self.title)
