from rest_framework import serializers
from .models import Storage,Image,File


class ImageSerializers(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = '__all__'

class StorageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Storage
        fields = ('id', 'file')
        read_only_fields = ('id',)



class FileSerializers(serializers.ModelSerializer):
    class Meta:
        model = File
        fields = '__all__'
