from rest_framework import viewsets
from .serializers import ImageSerializers,StorageSerializer,FileSerializers
from . import models
from django.db import transaction
from rest_framework.response import Response
from django.core.files.base import ContentFile
import base64
import io
from PIL import Image
import os



# import os
# from .models import Storage
# from rest_framework import status
# from rest_framework import viewsets
# from .serializers import StorageSerializer
# from rest_framework.response import Response
# from rest_framework.permissions import IsAuthenticated, AllowAny


def base64_file(data, name=None):
    _format, _img_str = data.split(';base64,')
    _name, ext = _format.split('/')
    if not name:
        name = _name.split(":")[-1]
    return ContentFile(base64.b64decode(_img_str),
                       name='{}.{}'.format(name, ext))


class ImageViewSet(viewsets.ModelViewSet):
    authentication_classes = []
    permission_classes = []
    queryset = models.Image.objects.all()
    serializer_class = ImageSerializers

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        # print(imgData)
        data = request.data
        print(data['type'])
        # print(data)
        # for item in models.Image.objects.all():
        # 	item.delete()

        if data['type'] == 'create':
            imgData = base64_file(data=data['data_url'], name=data['name'])
            model = models.Image()
            model.image = imgData
            model.save()
            return Response({'image': model.image.url, 'id': model.id})
        elif data['type'] == 'update':
            image_path = data['image_url']
            print(image_path)
            image_path = image_path.split('/media/')[1]
            """
			instance=models.Image.objects.get(id=int(data['image_id']))

			We Can use id as well but here we are using image link by analysing the future work

			"""
            instance = models.Image.objects.filter(image=image_path)[0]
            print(instance.image)
            BASE_DIR = os.path.dirname(
                os.path.dirname(os.path.abspath(__file__)))
            image_file_actual_path = BASE_DIR + data['image_url']
            if os.path.exists(image_file_actual_path):
                imgData = base64_file(data=data['data_url'], name=data['name'])
                instance.image = imgData
                instance.save()
                os.remove(image_file_actual_path)
                print('updated')
                print(instance.image.url)
                return Response({'image': instance.image.url, 'id': instance.id})
        elif data['type'] == 'delete':
            image_path = data['image_url']
            image_path = image_path.split('/media/')[1]
            """
			instance=models.Image.objects.get(id=int(data['image_id']))

			We Can use id as well but here we are using image link by analysing the future work

			"""
            instance = models.Image.objects.filter(image=image_path)[0]
            print(instance.image)
            BASE_DIR = os.path.dirname(
                os.path.dirname(os.path.abspath(__file__)))
            image_file_actual_path = BASE_DIR + data['image_url']
            if os.path.exists(image_file_actual_path):
                instance.delete()
                os.remove(image_file_actual_path)
                print('deleted')
                print(instance.image.url)
                return Response({'message': 'Image Deleted'})

        return Response({'image': 'No image Found out Generated'})

class EditorImageViewSet(viewsets.ModelViewSet):
    authentication_classes = []
    permission_classes = []
    queryset = models.Image.objects.all()
    serializer_class = ImageSerializers

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        # print(imgData)
        data = request.data
        model = models.Image()
        model.image = data['image']
        model.save()
        print(data['image'])
        return Response({'image': f'{model.image.url}'})




class StorageViewSet(viewsets.ModelViewSet):
    """
    A viewset for viewing, searching and editing Storage instances.
    """
    serializer_class = StorageSerializer
    queryset = models.Storage.objects.all().reverse()
    lookup_field = 'file_name'
    permission_classes = []
    http_method_names = ['post', 'put']

    def update(self, request, file_name):
        file = self.get_object()
        old_img = file.file.path
        serializer = StorageSerializer(file, data=request.data)
        if serializer.is_valid():
            serializer.save()
            file.file_name = file.file.name.split("/")[-1].replace(".", "-")
            file.save()
            if os.path.exists(old_img):
                os.remove(old_img)

            return Response(serializer.data, status=201)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)





class FileViewSet(viewsets.ModelViewSet):
    authentication_classes = []
    permission_classes = []
    queryset = models.File.objects.all()
    serializer_class = FileSerializers

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        data = request.data
        if data['type'] == 'create':
            model = models.File()
            model.file = data['file']
            model.name = data['name']
            model.save()
            return Response({'file': model.file.url, 'id': model.id, 'name': model.name})
        if data['type'] == 'delete':
            print(data)
            file_path = data['file_url']
            file_path = file_path.split('/media/')[1]
            """
            instance = models.File.objects.filter(file=file_path)[0]


            We Can use id as well but here we are using file link by analysing the future work

            """
            instance = models.File.objects.get(id=int(data['id_url']))
            print(instance.file)
            BASE_DIR = os.path.dirname(
                os.path.dirname(os.path.abspath(__file__)))
            file_file_actual_path = BASE_DIR + data['file_url']
            if os.path.exists(file_file_actual_path):
                instance.delete()
                os.remove(file_file_actual_path)
                return Response({'message': 'Image Deleted'})
        return Response({'file': 'No file Found out Generated'})
    
    
   


class GetFilesData(viewsets.ModelViewSet):
    authentication_classes = []
    permission_classes = []
    queryset = models.File.objects.all()
    serializer_class = FileSerializers

    @transaction.atomic
    def create(self, request, *args, **kwargs):
        print(request.data)
        data = request.data['data']
        response = []
        for item in data:
            file_path = item
            file_path = file_path.split('/media/')[1]
            instance = models.File.objects.filter(file=file_path)[0]
            temp = {
                'file': instance.file.url,
                'name': instance.name,
                'id': instance.id,
                'url': ''
            }
            response.append(temp)
        return Response({'data': response})
