from django.db import models
from django.contrib.postgres.fields import ArrayField

# Create your models here.


class Article(models.Model):
    ARTICLE_TYPES = (
        ('1', 'For Patient'),
        ('2', 'For Optometrist'),
        ('3', 'For Both'),
    )

    title = models.CharField(max_length=250)
    image = ArrayField(
        models.CharField(max_length=512),
        blank=True,
        null=True
    )
    file = ArrayField(
        models.CharField(max_length=512),
        blank=True,
        null=True
    )
    type = models.CharField(
        max_length=1, choices=ARTICLE_TYPES)
    description = models.TextField()

    link_name = models.CharField(max_length=250,blank=True,null=True)
    link = models.URLField(max_length=250,blank=True,null=True)
    posteddate = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('posteddate',)

    def __str__(self):
        return str(self.pk)
