from django.shortcuts import render
from rest_framework import viewsets
from .models import Advertisement
from .serializers import AdvertisementSerializer
from rest_framework.permissions import IsAuthenticated, AllowAny

# Advertisement Viewset


class AdvertisementViewSet(viewsets.ModelViewSet):
    """
    A viewset for viewing and editing advertisement instances.
    """
    serializer_class = AdvertisementSerializer
    queryset = Advertisement.objects.all().reverse()

    def get_permissions(self):
        """Set custom permissions for each action."""
        if self.action in ["list", "retrieve"]:
            self.permission_classes = [AllowAny, ]
        else:
            self.permission_classes = [IsAuthenticated, ]
        return super().get_permissions()
