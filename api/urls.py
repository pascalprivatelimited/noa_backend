from django.urls import include, path
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView
)

urlpatterns = [
    path('auth/user/', include('account.urls')),
    path('', include('blog.urls')),
    path('storage/', include('storage.urls')),
    path('optometrist/', include('optometrist.urls')),
    path('advertisement/', include('advertisement.urls')),
    path('token/', TokenObtainPairView.as_view(), name='token'),
    path('token/verify/', TokenVerifyView.as_view(), name='verify_token'),
    path('token/refresh/', TokenRefreshView.as_view(), name='refresh_token')
]
