from .models import Article
from rest_framework import status
from .serializers import ArticleSerializer
from rest_framework.response import Response
from storage.utils import deleteFileWithUrls
from rest_framework import viewsets, generics, filters
from rest_framework.permissions import IsAuthenticated, AllowAny
# Create your views here.
from django.db import transaction
from storage.utils import deleteMultipalImageWithInstance,deleteMultipalFileWithInstance


class ArticleViewSet(viewsets.ModelViewSet):
    """
    A viewset for viewing and editing Article instances.
    """
    queryset = Article.objects.all().reverse()
    serializer_class = ArticleSerializer
    search_fields = ['title']
    filter_backends = (filters.SearchFilter,)

    def get_permissions(self):
        """Set custom permissions for each action."""
        if self.action in ["list", "retrieve"]:
            self.permission_classes = [AllowAny, ]
        else:
            self.permission_classes = [IsAuthenticated, ]
        return super().get_permissions()




    @transaction.atomic
    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        temp=deleteMultipalFileWithInstance(instance,False)
        if(deleteMultipalImageWithInstance(instance) and temp):
            return Response(status=status.HTTP_204_NO_CONTENT)
        return Response(status=status.HTTP_404_NOT_FOUND)



class BlogSearchFilter(filters.SearchFilter):
    search_param = "title"


class GetPatientListViewSet(generics.ListAPIView):
    """
        A viewset for getting and searching optometrists.
    """
    queryset = Article.objects.filter(type__in=[1, 3]).reverse()
    serializer_class = ArticleSerializer
    search_fields = ['title']
    filter_backends = (filters.SearchFilter,)


class GetOptometristListViewSet(generics.ListAPIView):
    """
    A viewset for getting and searching optometrists.
    """
    queryset = Article.objects.filter(type__in=[2, 3]).reverse()
    serializer_class = ArticleSerializer
    search_fields = ['title']
    filter_backends = (filters.SearchFilter,)
