# noa_backend

Backend for NOA

## Setup

```shell
    #sqlite is taken as default db
    export DATABASE_URL="" #for postgres

```

## Heroku

```

API_URL = https://noa-api.herokuapp.com
ADMIN_URL = https://noa-api.herokuapp.com/admin
Admin Credentials:
email: admin@admin.com
password: admin

```

## Installation

```shell
    # create an environment i.e. venv
    python -m venv venv

    # activate the environment
    source venv/bin/activate

    #for windows
    source venv/Scripts/activate

    # install dependencies
    pip install -r requirements.txt

    # Migrate to db
    python manage.py migrate

    # run server
    python manage.py runserver

    # run test
    python manage.py test

    # run detailed test
    python manage.py test -v2

```

## Directory structure

    .
    ├── noa/                # core project folder
    ├── api/                # api project folder
    ├── account/            # app for handling accounts/users
    ├── blog/               # app for handling blogs
    ├── optometrist/        # app for handling optometrist
    ├── storage/            # app for uploading file/image
    ├── manage.py
    ├── README.md
    └── requirements.txt

## API Reference (Documentation)

URL : https://documenter.getpostman.com/view/8091590/UyrGCEW6

    - Types
            ('1', 'For Patient')

            ('2', 'For Optometrist')

            ('3', 'For Both')

## Project status

Completed
