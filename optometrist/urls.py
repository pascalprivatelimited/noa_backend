from .views import OptometristViewSet
from django.urls import path, include
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'', OptometristViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
