from django.test import TestCase
from rest_framework import status
from django.contrib.auth import get_user_model

User = get_user_model()


class OptometristTests(TestCase):
    header = None

    def setUp(self):
        user = User.objects.create_user(email='user@email.com', name='user', password='foo')
        response = self.client.post('/api/v1/token/', {'email': user.email, 'password': 'foo'})
        self.header = {'HTTP_AUTHORIZATION': f'Bearer {response.data["access"]}'}

    file_name = 'media/tmp/article1.png'
    data = {
        "file": open(file_name, "rb"),
    }

    def test_to_upload_a_file(self):
        # Issue a post request.
        response = self.client.post('/api/v1/storage/', self.data, **self.header)
