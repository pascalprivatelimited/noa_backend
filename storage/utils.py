import os
from storage.models import Storage,Image,File


def deleteFileWithUrls(urls):
    for url in urls:
        try:
            file_name = url.split("/")[-1].replace(".", "-")
            file = Storage.objects.filter(file_name=file_name)[0]
            if file:
                old_img = file.file.path
                if os.path.exists(old_img):
                    os.remove(old_img)
                file.delete()
        except Exception as e:
            print("Some Error occured while removing image", e)



def deleteImageWithInstance(instance):
    image_path = instance.image.split('/media/')[1]
    image = Image.objects.filter(image=image_path)[0]
    BASE_DIR = os.path.dirname(
        os.path.dirname(os.path.abspath(__file__)))
    image_file_actual_path = BASE_DIR + instance.image
    if os.path.exists(image_file_actual_path):
        image.delete()
        instance.delete()
        os.remove(image_file_actual_path)
    return True


def deleteMultipalImageWithInstance(instance,delete=True):
    for item in instance.image:
        image_path = item.split('/media/')[1]
        image = Image.objects.filter(image=image_path)[0]
        BASE_DIR = os.path.dirname(
            os.path.dirname(os.path.abspath(__file__)))
        image_file_actual_path = BASE_DIR + item
        if os.path.exists(image_file_actual_path):
            image.delete()
            os.remove(image_file_actual_path)
    if delete:
        instance.delete()
    return True



def deleteFileWithInstance(instance):
    file_path = instance.file.split('/media/')[1]
    file = File.objects.filter(file=file_path)[0]
    BASE_DIR = os.path.dirname(
        os.path.dirname(os.path.abspath(__file__)))
    file_file_actual_path = BASE_DIR + instance.file
    if os.path.exists(file_file_actual_path):
        file.delete()
        instance.delete()
        os.remove(file_file_actual_path)
    return True


def deleteMultipalFileWithInstance(instance,delete=True):
    for item in instance.file:
        file_path = item.split('/media/')[1]
        file = File.objects.filter(file=file_path)[0]
        BASE_DIR = os.path.dirname(
            os.path.dirname(os.path.abspath(__file__)))
        file_file_actual_path = BASE_DIR + item
        if os.path.exists(file_file_actual_path):
            file.delete()
            os.remove(file_file_actual_path)
    if delete:
        instance.delete()
    return True